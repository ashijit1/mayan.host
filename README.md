# mayan.host

This project seeks to eliminate toil involved in integrating open source tools for :

1. developing secure, good quality (code reviewed) software
2. building CI/CD pipelines for code
3. securely hosting, scaling, monitoring software after deployment.